require_relative './../../range_collection/range'

describe RangeCollection::Range do
  subject(:range) { RangeCollection::Range.new(5, 10) }

  context 'When testing the Range class' do
    it "should return true when the new_range on the right of current_range" do
      new_range = RangeCollection::Range.new(12, 13)
      expect(range.on_the_left_of?(new_range)).to eq true
    end

    it "should return false when the new_range is not on the right of current_range" do
      new_range = RangeCollection::Range.new(1, 3)
      expect(range.on_the_left_of?(new_range)).to eq false
      new_range = RangeCollection::Range.new(1, 6)
      expect(range.on_the_left_of?(new_range)).to eq false
    end

    it "should return true when the new_range on the left of current_range" do
      new_range = RangeCollection::Range.new(1, 3)
      expect(range.on_the_right_of?(new_range)).to eq true
    end

    it "should return false when the new_range is not on the left of current_range" do
      new_range = RangeCollection::Range.new(12, 13)
      expect(range.on_the_right_of?(new_range)).to eq false
      new_range = RangeCollection::Range.new(8, 13)
      expect(range.on_the_right_of?(new_range)).to eq false
    end

    it "should return true when the new_range has no intersection with current_range" do
      new_range = RangeCollection::Range.new(1, 4)
      expect(range.has_no_intersection_with?(new_range)).to eq true
      new_range = RangeCollection::Range.new(11, 13)
      expect(range.has_no_intersection_with?(new_range)).to eq true
    end

    it "should return false when the new_range has intersection with current_range" do
      new_range = RangeCollection::Range.new(10, 13)
      expect(range.has_no_intersection_with?(new_range)).to eq false
      new_range = RangeCollection::Range.new(8, 10)
      expect(range.has_no_intersection_with?(new_range)).to eq false
      new_range = RangeCollection::Range.new(1, 5)
      expect(range.has_no_intersection_with?(new_range)).to eq false
    end

    it "should return true when the new_range has intersection with current_range" do
      new_range = RangeCollection::Range.new(1, 6)
      expect(range.has_intersection_with?(new_range)).to eq true
      new_range = RangeCollection::Range.new(8, 10)
      expect(range.has_intersection_with?(new_range)).to eq true
      new_range = RangeCollection::Range.new(1, 5)
      expect(range.has_intersection_with?(new_range)).to eq true
      new_range = RangeCollection::Range.new(10, 13)
      expect(range.has_intersection_with?(new_range)).to eq true
    end

    it "should return false when the new_range has no intersection with current_range" do
      new_range = RangeCollection::Range.new(1, 4)
      expect(range.has_intersection_with?(new_range)).to eq false
      new_range = RangeCollection::Range.new(11, 13)
      expect(range.has_intersection_with?(new_range)).to eq false
    end

    it "should return true when the new_range cover with current_range" do
      new_range = RangeCollection::Range.new(6, 8)
      expect(range.cover?(new_range)).to eq true
    end

    it "should return false when the new_range not cover current_range" do
      new_range = RangeCollection::Range.new(10, 13)
      expect(range.cover?(new_range)).to eq false
      new_range = RangeCollection::Range.new(3, 9)
      expect(range.cover?(new_range)).to eq false
      new_range = RangeCollection::Range.new(9, 13)
      expect(range.cover?(new_range)).to eq false
      new_range = RangeCollection::Range.new(5, 6)
      expect(range.cover?(new_range)).to eq false
    end

    it "should return true when the new_range has left intersection with current_range" do
      new_range = RangeCollection::Range.new(7, 10)
      expect(range.has_left_intersection_with?(new_range)).to eq true
      new_range = RangeCollection::Range.new(8, 11)
      expect(range.has_left_intersection_with?(new_range)).to eq true
    end

    it "should return false when the new_range has no left intersection with current_range" do
      new_range = RangeCollection::Range.new(6, 8)
      expect(range.has_left_intersection_with?(new_range)).to eq false
      new_range = RangeCollection::Range.new(5, 6)
      expect(range.has_left_intersection_with?(new_range)).to eq false
      new_range = RangeCollection::Range.new(3, 9)
      expect(range.has_left_intersection_with?(new_range)).to eq false
    end

    it "should return true when the new_range has right intersection with current_range" do
      new_range = RangeCollection::Range.new(5, 6)
      expect(range.has_right_intersection_with?(new_range)).to eq true
      new_range = RangeCollection::Range.new(3, 6)
      expect(range.has_right_intersection_with?(new_range)).to eq true
    end

    it "should return false when the new_range has no right intersection with current_range" do
      new_range = RangeCollection::Range.new(6, 8)
      expect(range.has_right_intersection_with?(new_range)).to eq false
      new_range = RangeCollection::Range.new(8, 13)
      expect(range.has_right_intersection_with?(new_range)).to eq false
      new_range = RangeCollection::Range.new(5, 10)
      expect(range.has_right_intersection_with?(new_range)).to eq false
    end
  end
end