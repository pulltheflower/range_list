require_relative '../range_list'

describe RangeList do
  subject(:rl) { RangeList.new }

  context 'When testing the RangeList class' do
    it "should add new range to range_list" do
      rl.add([1, 5])
      expect(rl.print).to eq "[1, 5)"
    end

    it "should add new range to range_list" do
      rl.add([1, 5])
      rl.add([10, 20])
      expect(rl.print).to eq "[1, 5) [10, 20)"
    end

    it "should update the second range of range_list" do
      rl.add([1, 5])
      rl.add([10, 20])
      rl.add([20, 21])
      expect(rl.print).to eq "[1, 5) [10, 21)"
    end

    it "should not change range_list when a range contains the new_range" do
      rl.add([1, 5])
      rl.add([10, 20])
      rl.add([20, 21])
      rl.add([2, 4])
      expect(rl.print).to eq "[1, 5) [10, 21)"
    end

    it "should change the list that has intersection with the new_range " do
      rl.add([1, 5])
      rl.add([10, 20])
      rl.add([20, 21])
      rl.add([2, 4])
      rl.add([3, 8]) 
      expect(rl.print).to eq "[1, 8) [10, 21)"
    end

    it "should not remove any ranges" do
      rl.add([1, 5])
      rl.add([10, 20])
      rl.add([20, 21])
      rl.add([2, 4])
      rl.add([3, 8]) 
      rl.remove([10, 10])
      expect(rl.print).to eq "[1, 8) [10, 21)"
    end

    it "should change the second range" do
      rl.add([1, 5])
      rl.add([10, 20])
      rl.add([20, 21])
      rl.add([2, 4])
      rl.add([3, 8]) 
      rl.remove([10, 10]) 
      rl.remove([10, 11])
      expect(rl.print).to eq "[1, 8) [11, 21)"
    end

    it "should separate the second range" do
      rl.add([1, 5])
      rl.add([10, 20])
      rl.add([20, 21])
      rl.add([2, 4])
      rl.add([3, 8]) 
      rl.remove([10, 10]) 
      rl.remove([10, 11])
      rl.remove([15, 17])
      expect(rl.print).to eq "[1, 8) [11, 15) [17, 21)"
    end

    it "should remove the second range and change the third range" do
      rl.add([1, 5])
      rl.add([10, 20])
      rl.add([20, 21])
      rl.add([2, 4])
      rl.add([3, 8]) 
      rl.remove([10, 10]) 
      rl.remove([10, 11])
      rl.remove([15, 17])
      rl.remove([3, 19])
      expect(rl.print).to eq "[1, 3) [19, 21)"
    end
  end
end