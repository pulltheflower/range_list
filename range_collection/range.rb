# frozen_string_literal: true

module RangeCollection
  class Range
    attr_accessor :left, :right
    def initialize(left, right)
      @left = left
      @right = right
    end

    # like [2, 3) and [5, 10)
    def on_the_left_of?(range)
      self.right < range.left 
    end

    # like [5, 10) and [1, 3)
    def on_the_right_of?(range)
      self.left > range.right
    end

    # like [2, 3) and [5, 10)
    # or [5, 10) and [1, 3)
    def has_no_intersection_with?(range)
      (on_the_left_of?(range) || on_the_right_of?(range)) && (self.right != range.left && self.left != range.right)
    end

    # like [2, 6) and [3, 9)
    # or [2, 6) and [1, 7)
    # or [2, 6) and [1, 3)
    # or [2, 6) and [4, 7)
    def has_intersection_with?(range)
      self.left <= range.right && self.right >= range.left
    end

    # like [5, 10) and [6, 7)
    def cover?(range)
      self.left < range.left && self.right > range.right
    end

    # like [4, 6) and [5, 7) 
    # or [4, 6) and [5, 6)
    def has_left_intersection_with?(range)
      has_intersection_with?(range) && self.left < range.left && self.right <= range.right
    end

    # like [4, 6) and [3, 5)
    # or [4, 6) and [4, 5)
    def has_right_intersection_with?(range)
      has_intersection_with?(range) && self.left >= range.left && self.right > range.right
    end
  end
end