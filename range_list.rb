# #frozen_string_literal: true

require_relative './range_collection/range'

class RangeList
  attr_accessor :range_list

  ARGUMENT_ERROR_MESSAGE = "Argument should be an array with two integer items and the first item should less equal than the second item"

  def initialize
    @range_list = []
  end

  def add(range)
    validate_params(range)

    result = []
    new_range = RangeCollection::Range.new(range[0], range[1])

    if range_list.empty?
      result << new_range
    else
      index = 0
      # find the ranges that on the new_range's left
      while (index < range_list.length && range_list[index].on_the_left_of?(new_range)) do
        result << range_list[index]
        index = index + 1
      end

      # find the ranges that has an intersection with new_rangs and merge them
      while (index < range_list.length && range_list[index].has_intersection_with?(new_range)) do
        new_range.left = [new_range.left, range_list[index].left].min
        new_range.right = [new_range.right, range_list[index].right].max
        index = index + 1 
      end

      result << new_range

      # find the ranges that on the new_range's right
      while (index < range_list.length) do
        result << range_list[index]
        index = index + 1
      end
    end
    self.range_list = result
  end

  def remove(range)
    validate_params(range)
    
    return if range_list.empty?
    new_range = RangeCollection::Range.new(range[0], range[1])

    result = []
    range_list.each do |range|
      # add new_range if the new_range has no intersection with any ranges
      if range.has_no_intersection_with?(new_range)
        result << range
        next
      end

      # separate ranges that has intersection with the new_range
      if range.has_right_intersection_with?(new_range)
        result << RangeCollection::Range.new(new_range.right, range.right)
      elsif range.has_left_intersection_with?(new_range)
        result << RangeCollection::Range.new(range.left, new_range.left)
      elsif range.cover?(new_range)
        result << RangeCollection::Range.new(range.left, new_range.left)
        result << RangeCollection::Range.new(new_range.right, range.right)
      end
    end
    self.range_list = result
  end

  def print
    strs = []
    range_list.each do |range|
      strs << "[#{range.left}, #{range.right})"
    end
    strs.join(" ")
  end

  private 

  def validate_params(range)
    unless range.is_a?(Array) && range.length == 2 && range[0].is_a?(Integer) && range[1].is_a?(Integer) && range[0] <= range[1]
      raise ArgumentError, ARGUMENT_ERROR_MESSAGE 
    end
  end
end