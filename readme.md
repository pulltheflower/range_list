## RangeList

### Environments

```shell
  ruby
  rspec
```

### Usage
A pair of integers define a range, for example: [1, 5). This range includes integers: 1, 2, 3, and 4. 
A range list is an aggregate of these ranges: [1, 5), [10, 11), [100, 201) 

You can create a new range list with `RangeList.new` and you can add new range to the list by using the `add` method, remove range from the range list should using the `remove` method. You can also print the range list by using the `print` method.

Here are some examples
```ruby
require 'range_list'

rl = RangeList.new

rl.add([1, 5]) 
rl.print # Should display: [1, 5) 
rl.add([10, 20]) 
rl.print # Should display: [1, 5) [10, 20) 
rl.add([20, 20]) 
rl.print # Should display: [1, 5) [10, 20) 
rl.add([20, 21]) 
rl.print # Should display: [1, 5) [10, 21) 
rl.add([2, 4]) 
rl.print # Should display: [1, 5) [10, 21) 
rl.add([3, 8]) 
rl.print # Should display: [1, 8) [10, 21) 
rl.remove([10, 10]) 
rl.print # Should display: [1, 8) [10, 21) 
rl.remove([10, 11]) 
rl.print # Should display: [1, 8) [11, 21) 
rl.remove([15, 17]) 
rl.print # Should display: [1, 8) [11, 15) [17, 21) 
rl.remove([3, 19]) 
rl.print # Should display: [1, 3) [19, 21)
```

### Test

```shell
  rspec spec/range_list_spec.rb
  rspec spec/range_collection/range_spec.rb
```